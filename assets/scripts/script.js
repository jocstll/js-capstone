
let boxes = [];

addBtn.addEventListener('click', ()=>{
    let name = document.getElementById('friendName');
    document.getElementById('box-container').style.display = 'block';
    if(name.value != ""){
        // push the input name into boxes
        boxes.push(name.value);
        // to reset the input name
        name.value = "";


        for (i=0; i<boxes.length; i++) {
                   
            var box = document.createElement('div');
            box.className = 'boxes animated zoomIn text-center bg-danger text-white font-weight-bold text-uppercase mb-3 form-control w-75 mx-auto';
            box.style.wordBreak = "break-all";


            let spanName = document.createElement('span');
            spanName.className = 'text-black';
            spanName.innerHTML = boxes[i];
            box.appendChild(spanName);
        }
        // for positioning
         document.getElementById('box-container').appendChild(box);
    }else{
        alert("Add a candidate")
    }
});


goBtn.addEventListener('click', () => {
   
    let randomItem = boxes[Math.floor(Math.random() * boxes.length)];

    let appendedBoxes = document.querySelectorAll('.boxes');
    for (i=0; i<appendedBoxes.length; i++) {
        appendedBoxes[i].className = 'animated zoomOut';
    }
    if(boxes.length != 0){
    setTimeout( () => { 

        for (i=0; i<appendedBoxes.length; i++) {
            appendedBoxes[i].parentNode.removeChild(appendedBoxes[i]);
        }
        let modalCongrats = document.getElementById('modalBody');

        modalCongrats.innerHTML = randomItem;
        boxes = [];

        $("#modalResult").modal('show');

    }, 500);
        
    }else{
        alert("Add a candidate");
    }
});
